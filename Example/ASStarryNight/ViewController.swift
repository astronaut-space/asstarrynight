//
//  ViewController.swift
//  ASStarryNight
//
//  Created by astronaut on 01/12/2022.
//  Copyright (c) 2022 astronaut. All rights reserved.
//

import UIKit
import ASStarryNight

import SwiftyBeaver
import SQLite

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: 操作
    @IBAction func testGreekLetterConversion(_ sender: Any) {
        
        let a = String(describing: GreekLetter(shortEnglish: "Alp"))
        let ω = String(describing: GreekLetter(shortEnglish: "Ome"))
        let γ = String(describing: GreekLetter(shortEnglish: "Gam"))
        let ι = String(describing: GreekLetter(shortEnglish: "Iot"))
        let π = String(describing: GreekLetter(shortEnglish: "Pi"))
        let τ = String(describing: GreekLetter(shortEnglish: "Tau"))
        print(a, ω, γ, ι, π, τ)
    }
    
    @IBAction func testBayerFlamsteedConversion(_ sender: Any) {
        guard let bf1 = BayerFlamsteed("10Iot2Cyg"),
                let bf2 = BayerFlamsteed("Sig Oct"),
                let bf3 = BayerFlamsteed("32Xi Dra") else { return }
        print(String(describing: bf1), String(describing: bf2), String(describing: bf3))
    }
    
    @IBAction func testAllBayerFlamsteedConversions(_ sender: Any) {
        Star.magitudeLessThan(7).compactMap { $0.identity.rawBfDesignation }.forEach {
            print(BayerFlamsteed($0) ?? "?")
        }
    }
    
}

