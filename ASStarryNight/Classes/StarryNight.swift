//
//  StarryNight.swift
//  Graviton
//
//  Created by Sihao Lu on 8/12/17.
//  Copyright © 2017 Ben Lu. All rights reserved.
//

import SwiftyBeaver
import SQLite

let logger = SwiftyBeaver.self

// 没有实际作用的类，主要用来调用bundle
public class StarryNight { }

let StarryNightBundle: Bundle = {
    guard let url = Bundle(for: StarryNight.self).url(forResource: "ASStarryNight", withExtension: "bundle"),
          let bundle = Bundle(url: url) else {
              return Bundle.main
          }
    return bundle
}()

let db = try! Connection(StarryNightBundle.path(forResource: "stars", ofType: "sqlite3")!)

public let constellationLinePath = StarryNightBundle.path(forResource: "constellation_lines", ofType: "dat")!
