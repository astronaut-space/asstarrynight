#
# Be sure to run `pod lib lint ASStarryNight.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ASStarryNight'
  s.version          = '1.0.1'
  s.summary          = 'A short description of ASStarryNight.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/astronaut-space/asstarrynight'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'astronaut' => 'z13976100@163.com' }
  s.source           = { :git => 'https://gitlab.com/astronaut-space/asstarrynight.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'

  s.source_files = 'ASStarryNight/Classes/**/*'
  
   s.resource_bundles = {
     'ASStarryNight' => ['ASStarryNight/Assets/*.{sqlite3,dat}']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'SwiftyBeaver'
   s.dependency 'SQLite.swift'
   s.dependency 'STRegex'
   s.dependency 'ASSpaceTime'
end
